import Foundation
print("Set username")
let user = readLine() //"sergeyblago"

if let user = user,
  user.isEmpty == false {
  DispatchQueue.global(qos: .background).async {
    NetworkManager().requestRepositoryFromUser(username: user,
                                               successClosure: { repos in
                                                print("\(user) repositories: ")
                                                for repo in repos {
                                                  print(repo.name)
                                                }
                                                exit(EXIT_SUCCESS)
    },
                                               errorClosure: { reason in
                                                print(reason)
                                                exit(EXIT_SUCCESS)
    })
    
  }
} else {
  print("Error name")
  exit(EXIT_FAILURE)
  
}

dispatchMain()
