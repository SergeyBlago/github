import Alamofire
import Foundation

struct Repository: Codable {
  var name: String
}

class NetworkManager {
  func requestRepositoryFromUser(username: String, successClosure: @escaping ([Repository]) -> Void, errorClosure: @escaping (String) -> Void) {
    let urlString = "https://api.github.com/users/\(username)/repos"
    
    Alamofire.request(urlString).response { response in
      guard let data = response.data else {
        errorClosure("Connect error")
        return
      }
      
      do {
        let parsedResult: [Repository] = try JSONDecoder().decode([Repository].self, from: data)
        successClosure(parsedResult)
      } catch {
        errorClosure("Decode error")
      }
    }
  }
}


