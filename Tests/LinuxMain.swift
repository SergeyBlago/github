import XCTest

import github_alamofireTests

var tests = [XCTestCaseEntry]()
tests += github_alamofireTests.allTests()
XCTMain(tests)
