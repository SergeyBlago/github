import XCTest

#if !os(macOS)
    public func allTests() -> [XCTestCaseEntry] {
        return [
            testCase(github_alamofireTests.allTests),
        ]
    }
#endif
